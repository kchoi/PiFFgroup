import argparse
import ply.lex as lex
import ply.yacc as yacc
import json

class Word(str):
    pass


class Variable(str):
    pass


tokens = (
    'LPAREN',
    'RPAREN',
    'LBRACKET',
    'RBRACKET',
    'BAR',
    'COMMA',
    'DOT',
    'INTEGER',
    'FLOAT',
    'STRING',
    'WORD',
    'VARIABLE',
)


t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACKET = r'\['
t_RBRACKET = r'\]'
t_BAR = r'\|'
t_COMMA = r','
t_DOT = r'\.'
t_ignore = ' '


def t_FLOAT(t):
    r'[+-]?(\d+\.(\d*)?|\.\d+)'
    t.value = float(t.value)
    return t


def t_INTEGER(t):
    r'[+-]?\d+'
    t.value = int(t.value)
    return t


def t_VARIABLE(t):
    r'_?[A-Z]\w*'
    t.value = Variable(t.value)
    return t


def t_WORD(t):
    r'\w+'
    t.value = Word(t.value)
    return t


def t_STRING(t):
    r'\"([^\\\n]|(\\.))*?\"'
    t.value = t.value[1:-1]
    return t


def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)


# Error handling rule
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


def p_rule(p):
    'rule : expression DOT'
    p[0] = p[1]
    return p


def p_expression(p):
    'expression : LPAREN WORD expressions RPAREN'
    p[0] = {p[2]: p[3]}
    return p


def p_atom(p):
    '''atom : WORD
            | INTEGER
            | FLOAT
            | STRING
            | VARIABLE
            | list
            | expression'''
    p[0] = p[1]
    return p


def p_expressions(p):
    '''expressions : atom
                   | atom expressions'''
    if len(p) == 2:
        p[0] = p[1]
    else:
        if isinstance(p[2], tuple):
            p[0] = (p[1], *p[2])
        else:
            p[0] = (p[1], p[2])
    return p


def p_list(p):
    '''list : LBRACKET items RBRACKET
            | LBRACKET RBRACKET'''
    if len(p) > 3:
        p[0] = p[2]
    else:
        p[0] = []
    return p


def p_items(p):
    '''items : atom
             | atom COMMA items
             | atom BAR VARIABLE'''
    if len(p) > 2:
        if p[2] == ',':
            if isinstance(p[3], list):
                p[0] = [p[1], *p[3]]
            else:
                p[0] = [p[1], p[3]]
        else:
            p[0] = [p[1]]
    else:
        p[0] = [p[1]]
    return p


def p_error(t):
    print("Syntax error at '%s'" % t.value)


class DmosWriter:

    def serialize(self, ast):
        return self._serialize(ast) + ".\n"

    def _serialize(self, ast):
        if isinstance(ast, dict):
            # serializing expression
            for key, value in ast.items():
                # there is only ever one key in a dict
                serialized_value = self._serialize(value)
                return f"({key} {serialized_value})"
        elif isinstance(ast, tuple):
            # serializing expressions
            return " ".join([self._serialize(value) for value in ast])
        elif isinstance(ast, list):
            serialized_values = ", ".join([self._serialize(value)
                                           for value in ast])
            return f"[{serialized_values}]"
        elif isinstance(ast, int):
            return f"{ast}"
        elif isinstance(ast, float):
            return f"{ast:.06f}"
        elif isinstance(ast, Word):
            return f"{ast}"
        elif isinstance(ast, Variable):
            return f"{ast}"
        elif isinstance(ast, str):
            return f'"{ast}"'
        else:
            # return f"{ast}"
            return "Unknown"


lexer = lex.lex()
dmos_parser = yacc.yacc()
dmos_writer = DmosWriter()


def toJson(name, ast):
    with open(name, 'w') as json_file:
        json.dump(ast, json_file)
        


def parse_file(name):
    with open(name) as f:
        mem_data = f.read()
    ast = dmos_parser.parse(mem_data)
    return ast


def main(args):
    ast = parse_file(args.input_file)
    serialized_ast = dmos_writer.serialize(ast)
    __import__('pprint').pprint(ast)
    print(serialized_ast)
    with open(args.input_file) as f:
        mem_data = f.read()
    assert mem_data == serialized_ast 
    out_file=args.out
    if out_file is not None :
        toJson(out_file, ast)
    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file")
    parser.add_argument('-o', '--out', required=False, type=str, default=None, help='output file')
    args = parser.parse_args()
    
    
    main(args)
