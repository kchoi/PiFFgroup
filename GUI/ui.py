#!/usr/bin/python3
#coding: utf-8

"""
Copyright Université de Nantes

Created on Tue Jun 22 2017
@author : Adeline Granet

@contact : Harold Mouchère

Generate user interface to valid transcriptions

"""
import tkinter
from tkinter import *
from tkinter import filedialog

from collections import deque

import itertools
from PIL  import Image as IG
from PIL import ImageTk
from rope.base.pyobjectsdef import _AssignVisitor

import Piff as pf
import os

global image
global trans_select
global workingDir

class Interface(Frame):


    """Notre fenêtre principale.
    Tous les widgets sont stockés comme attributs de cette fenêtre."""


    def __init__(self, fenetre, **kwargs):
        Frame.__init__(self, fenetre, width=768, height=576,**kwargs)

        self.pack(fill=BOTH)
        self.cpt = 0
        self.color_poly = {'textline':'blue','title':'red'}
        self.current_draw = None
        self.current_poly = None

        # architecture de la fenetre
        # frame 2
        self.Frame2 = Frame(fenetre,  relief=GROOVE,bg='white')
        # frame 3
        self.Frame3 = Frame(fenetre,relief=GROOVE,bg='white')
        self.Frame3.pack(fill=X, side=BOTTOM)
        # frame 4
        self.Frame4 = Frame(fenetre, relief=GROOVE)


        # canvas
        self.image_zone = Canvas(self.Frame2, width=800, height=800,scrollregion=(0, 0, 1000, 1000))
        vbar=Scrollbar(self.Frame2,orient=VERTICAL)
        vbar.pack(side=RIGHT,fill=Y, expand = FALSE)
        vbar.config(command=self.image_zone.yview)
        hbar=Scrollbar(self.Frame2, orient=HORIZONTAL)
        hbar.pack(side=BOTTOM,fill=X, expand =FALSE)
        hbar.config(command=self.image_zone.xview)
        self.image_zone.config(yscrollcommand=vbar.set)
        self.image_zone.config(xscrollcommand=hbar.set)

        # gadget
        self.bouton_save = Button(self.Frame3, text="Save", command=self.save_piff, bg='cyan')
        # self.bouton_save.pack(side=RIGHT)
        self.bouton_quitter = Button(self.Frame3, text="Quit", command=self.quit, bg='cyan')
        self.bouton_quitter.pack(side=RIGHT)

        self.bouton_load_image = Button(self, text="Open image...", fg="blue", command=self.display_image)
        self.bouton_load_piff = Button(self, text="Open piff file...", fg="blue", command=self.load_piff)
        self.bouton_load_image.pack(side="right")
        self.bouton_load_piff.pack(side="right")

        self.combo_transcription = Listbox(self.Frame4, width=200, height=10, selectmode=BROWSE)
        self.combo_transcription.bind('<<ListboxSelect>>',self.CurSelet)
        self.value = self.combo_transcription.get(ACTIVE)

        yscroll = Scrollbar(self.Frame4, orient=VERTICAL)
        yscroll.pack(side=RIGHT, fill=Y,expand=FALSE)
        self.combo_transcription.configure(yscrollcommand=yscroll.set)

        # use entry widget to display/edit selection
        self.enter1 = Entry(self.Frame4, width=200, bg='linen')
        self.enter1.insert(0, 'Click on an item in the listbox')
        self.bouton_suivant = Button(self.Frame4, text="Next", command=self.next_poly, bg='cyan')
        self.bouton_valide = Button(self.Frame4, text="Valid", command=self.valid_poly, bg='cyan')
        self.bouton_del_poly = Button(self.Frame4, text="Delete polygon...", command=self.change_poly, fg="firebrick1")


    def CurSelet(self,event):
        idx_select=self.combo_transcription.curselection()[0]
        trans_select = self.combo_transcription.get(idx_select)
        self.clear_text()
        self.enter1.insert(0,trans_select)

    def clear_text(self):
        self.enter1.delete(0, 'end')

    def delete_all_items(self):
        """
        delete all lines from the listbox
        """
        try:
            self.combo_transcription.delete(0,last=END)
        except IndexError:
            pass

    def copy_piff(self):
        self.new_piff.meta = self.original_piff.meta
        # self.new_piff.data = self.original_piff.data
        # self.new_piff.location = self.original_piff.location
        # print(self.new_piff)

    def load_piff(self):
        filepath = filedialog.askopenfilename(title="Open piff file",filetypes=[('json files','.json'),('piff files','.piff'),('all files','.*')])
        workingDir = os.path.dirname(os.path.abspath(filepath))
        print(filepath)
        self.original_piff = pf.Piff()
        self.new_piff = pf.Piff()
        self.original_piff.load(filepath)
        self.copy_piff()

        self.queues_id_polygons = deque()
        ###
        self.Frame2.pack(side=LEFT, fill =Y)
        self.Frame4.pack(fill=Y,side=RIGHT)
        self.bouton_suivant.pack(side=RIGHT)
        self.bouton_valide.pack(side=RIGHT)
        self.bouton_del_poly.pack(side=LEFT)
        self.combo_transcription.pack()
        self.enter1.pack()
        ###
        self.get_image(workingDir)
        self.get_transcriptions_by_polygon()
        self.coords_poly = {}
        self.get_all_polygons('title')
        self.get_all_polygons('textline')
        self.init_poly()

    def information_clear(self):
        self.delete_all_items()
        self.image_zone.delete(self.current_draw)
        self.clear_text()

    def next_poly(self):
        self.queues_id_polygons.append(self.current_poly)
        self.change_poly()

    def change_poly(self):
        if len(self.queues_id_polygons)> 0 :
            self.information_clear()
            self.current_poly = self.queues_id_polygons.popleft()
            self.draw_polygon2(self.coords_poly[self.current_poly], 'textline')
            self.fill_transcriptions(self.trans_poly[self.current_poly])
        if len(self.queues_id_polygons) == 0 :
            self.bouton_save.pack(side=RIGHT)
            self.bouton_suivant.pack_forget()
            self.bouton_valide.pack_forget()

    def valid_poly(self):
        # create data and location
        tmp_location = self.original_piff.get_location(self.current_poly)
        self.new_piff.add_elt_data(content=self.enter1.get(),data_id='av_'+str(self.cpt),location_id=self.current_poly+'_val',parent_id_list=None,data_type='annotation')
        self.new_piff.add_elt_location(location_id=self.current_poly+'_val',location_type=tmp_location['type'],polygon=tmp_location['polygon'])
        self.change_poly()
        self.cpt +=1



    def init_poly(self):
        self.current_poly = self.queues_id_polygons.popleft()
        self.draw_polygon2(self.coords_poly[self.current_poly], 'title')
        self.fill_transcriptions(self.trans_poly[self.current_poly])


    def save_piff(self):
        self.valid_poly()
        filepath = './new_'
        self.new_piff.save(filepath+self.original_piff.meta['id']+'.piff')

    def display_image(self):
        filepath = filedialog.askopenfilename(title="Ouvrir une image",filetypes=[('jpg files','.jpg'),('all files','.*')])
        img_original = IG.open(filepath)
        img_size_original = img_original.size
        coef = 2
        self.draw_image(img_original,coef, img_size_original[0],img_original[1])
        self.draw_polygon(coef)

    def draw_polygon(self,coef, polygon=None):
        self.image_zone.create_polygon(int(458/coef),int(301/coef),int(1573/coef),int(301/coef),int(1573/coef),int(507/coef),int(458/coef),int(507/coef),outline='red',tags='titre bloc', fill='')
        self.image_zone.pack(side=LEFT)

    def draw_polygon2(self, polygon, type_poly):
        self.current_draw = self.image_zone.create_polygon(polygon,outline=self.color_poly[type_poly],tags=type_poly, fill='')
        self.image_zone.pack(side=LEFT)

    def get_image(self, workDir = "."):
        img_original = IG.open( workDir+"\\" + self.original_piff.meta["url"] )
        img_size_original = img_original.size
        coef = 2.5
        self.draw_image(img_original, coef,int(img_size_original[0]), int(img_size_original[1]))

    def draw_image(self,image, coeff, x, y ):
        self.image = image.resize((int(x/coeff), int(y/coeff)),IG.BICUBIC )
        self.photo = ImageTk.PhotoImage(self.image)
        self.image_zone.create_image(0,0,image=self.photo,anchor='nw')
        self.image_zone.pack(side=LEFT, expand = TRUE)

    def get_all_polygons(self, type_location):
        dict_polygons = self.original_piff.location
        for poly in dict_polygons :
            if poly["type"] == type_location :
                my_poly = [float(x)/(2.5) for x in list(itertools.chain.from_iterable(poly['polygon']))]
                self.coords_poly[poly['id']] = my_poly
                self.queues_id_polygons.append(poly['id'])
                # self.draw_polygon2(my_poly, type_location)
                # self.fill_transcriptions(self.trans_poly[poly['id']])
        print(self.queues_id_polygons)

    def get_transcriptions_by_polygon(self):
        self.trans_poly = {}
        tmp_list = []
        for elt_data in self.original_piff.data :
            if elt_data['location_id'] in self.trans_poly.keys() :
                tmp_list = self.trans_poly[elt_data['location_id']]
                tmp_list.append(elt_data['value'])
                self.trans_poly[elt_data['location_id']] = tmp_list
            else :
                tmp = [elt_data['value']]
                self.trans_poly[elt_data['location_id']] = tmp

    def fill_transcriptions(self,list_trans):
        for elt in list_trans :
            self.combo_transcription.insert(END,elt)

if __name__ == '__main__':
    fenetre = Tk()
    fenetre.title("Validation des annotations")
    #fenetre.attributes('-fullscreen',True)
    fenetre['bg']='white'
    interface = Interface(fenetre)
    interface.mainloop()
    # interface.destroy()

