# coding: utf8
# authors : Christopher Kermorvant, Andres Rojas
# 2017 -  TEKLIA

"""
    This script uses a json databse and
    creates a .md file containing
    the characteristics from a database:
    *id
    *url
    *nb pages
    *nb paragraphs
    *nb line
    *nb words
    *nb charcters
"""

import json
import argparse
import sys

if __name__ == "__main__":

    description = ('generate the statistics in .md format from the chosen JSON')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-file', help='path of the JSON database to study')
    parser.add_argument('--output-file', help='path to save the new .md')
    args = parser.parse_args()
    args_dict = args.__dict__

    with open(args_dict["input_file"]) as data_file:
        data = json.load(data_file)

    total_pages = 0
    total_lines = 0
    total_char = 0
    total_words = 0
    total_paragraphs = 0

    def recursive_children(d):
        """
            goes through the different children elements
            in a recursive way and counts the number of
            elements
        """
        global total_paragraphs
        global total_lines
        global total_char
        global total_pages
        global total_words
        if "children" in d:
            for i in d["children"]:
                if i["type"] == "page":
                    total_pages += 1
                    words = str.split(i["value"])
                    words = list(filter(lambda a: a != ".", words))
                    words = list(filter(lambda a: a != ",", words))
                    total_words += len(words)
                    total_char += len(i["value"]) - i["value"].count(" ") - i["value"].count("\n")
                if i["type"] == "paragraph":
                    total_paragraphs += 1
                if i["type"] == "line":
                    total_lines += 1
                    if total_pages == 0:
                        words = str.split(i["value"])
                        words = list(filter(lambda a: a != ".", words))
                        words = list(filter(lambda a: a != ",", words))
                        total_words += len(words)
                        total_char += len(i["value"]) - i["value"].count(" ") - i["value"].count("\n")
                if "children" in i:
                    recursive_children(i)

    recursive_children(data)

    new_string = "# Base {} \n \n* url : {} \n* number of pages : {} \n* number of paragraphs : {} \n* number of lines : {} \n* number of words : {} \n* number of charcters : {}"
    new_string = new_string.format(data["id"],data["url"],total_pages,total_paragraphs,total_lines,total_words,total_char)

    with open(args_dict["output_file"], 'w') as outfile:
        outfile.write(new_string)
