# coding: utf8
# authors : Christopher Kermorvant, Andres Rojas
# 2017 -  TEKLIA

"""
    This script transform the CVL XML into a JSON
    with the values and characteristics for
    Lines and Paragraphs
"""

import json
import argparse
import sys
import xml.etree.ElementTree as et
from pathlib import Path
from collections import OrderedDict

if __name__ == "__main__":

    description = ('generate a JSON of the CVL XMLs')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-folder', help='path of the folder containing CVL XMLs to convert')
    parser.add_argument('--output-json', help='path to save the new Cvl json')
    args = parser.parse_args()
    args_dict = args.__dict__

    new_data = OrderedDict()
    new_data["type"] = "corpus"
    new_data["url"] = "https://www.caa.tuwien.ac.at/cvl/research/cvl-databases/an-off-line-database-for-writer-retrieval-writer-identification-and-word-spotting/"
    new_data["id"] = "CVL"
    new_data["children"] = []
    pathlist = Path(args_dict["input_folder"]).glob('**/*.xml')
    for path in sorted(pathlist):
        try:
            path_in_str = str(path)
            doc = et.parse(path_in_str).getroot()
            temp_paragraph = OrderedDict()
            temp_paragraph["type"] = "paragraph"
            temp_paragraph["value"] = ""
            temp_paragraph["children"] = []
            get_region = doc.findall("./Page/AttrRegion/")[2]
            for i in get_region[1:]:
                region_lines = i.findall("./AttrRegion")
                zone_line = i.findall("./minAreaRect")
                if not zone_line:
                    continue
                temp_line = OrderedDict()
                temp_line["type"] = "line"
                temp_line["value"] = ""
                for j in region_lines:
                    if j.get("text") is not None:
                        temp_line["value"] = temp_line["value"] + j.get("text") + " "
                #temp_line["value"] = temp_line["value"][:-1] uncomment to eliminate end spaces
                zone = [[int(zone_line[0][0].get("x")),int(zone_line[0][0].get("y"))],
                    [int(zone_line[0][1].get("x")),int(zone_line[0][1].get("y"))],
                    [int(zone_line[0][2].get("x")),int(zone_line[0][2].get("y"))],
                    [int(zone_line[0][3].get("x")),int(zone_line[0][3].get("y"))]]
                temp_line["zone"] = zone
                temp_paragraph["value"] = temp_paragraph["value"] + temp_line["value"] + "\n"
                temp_paragraph["children"].append(temp_line)
            temp_paragraph["value"] = temp_paragraph["value"][:-3]
            new_data["children"].append(temp_paragraph)
        except et.ParseError:
            continue

    with open(args_dict["output_json"], 'w') as outfile:
        json.dump(new_data, outfile, ensure_ascii=False)
