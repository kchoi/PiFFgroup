import json
import argparse
from collections import OrderedDict
from pathlib import Path

if __name__ == "__main__":

    description = ('generate a JSON of the SaintGall txts')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-folder', help='path of the folder containing the .txt to convert')
    parser.add_argument('--output-json', help='path to save the new SaintGall json')
    args = parser.parse_args()



    new_data = OrderedDict()
    new_data["type"] = "corpus"
    new_data["url"] = "http://www.fki.inf.unibe.ch/databases/iam-historical-document-database/saint-gall-database"
    new_data["id"] = "SaintGall"
    new_data["children"] = []

    with open(args.input_folder+'transcription.txt') as f:
        content = f.readlines()

    current_page = content[0].split(' ')[0][:-3]
    temp_page = OrderedDict()
    temp_page['type'] = 'page'
    temp_page['value'] = ''
    temp_page['path'] = 'data/page_images/' + current_page + '.jpg'
    temp_page['children'] = []

    for line in content:
        tmp = line.split(' ')[:2]
        ref = tmp[0]
        txt = tmp[1].replace('|',' ').replace('-','')
        temp_line = OrderedDict()
        temp_line['type'] = 'line'
        temp_line['value'] = txt
        temp_line['path'] = 'data/line_images_normalized/' + ref + '.png'
        page_id = ref[:-3]
        if current_page == page_id:
            temp_page['value'] += txt + '\n'
            temp_page['children'].append(temp_line)
        else:
            current_page = page_id
            new_data['children'].append(temp_page)
            del temp_page
            temp_page = OrderedDict()
            temp_page['type'] = 'page'
            temp_page['value'] = txt + '\n'
            temp_page['path'] = 'data/page_images/' + current_page + '.jpg'
            temp_page['children'] = []
            temp_page['children'].append(temp_line)
    new_data['children'].append(temp_page)

    with open(args.output_json, 'w') as outfile:
        json.dump(new_data, outfile, ensure_ascii=False)
