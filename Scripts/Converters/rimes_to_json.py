# coding: utf8
# authors : Christopher Kermorvant, Andres Rojas
# 2017 -  TEKLIA

"""
    This script transform the RIMES XML into a JSON
    with the values and characteristics for
    Lines and Paragraphs
"""

import json
import argparse
import sys
import xml.etree.ElementTree as et
from collections import OrderedDict

if __name__ == "__main__":

    description = ('generate a JSON of the RIMES XML')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-xml', help='path of the Rimes XML to convert')
    parser.add_argument('--output-json', help='path to save the new Rimes json')
    args = parser.parse_args()
    args_dict = args.__dict__

    doc = et.parse(args_dict["input_xml"]).getroot()

    new_data = OrderedDict()
    new_data["type"] = "corpus"
    new_data["url"] = "http://www.a2ialab.com/doku.php?id=rimes_database:start"
    new_data["id"] = "RIMES"
    new_data["children"] = []
    for i in doc:
        temp_paragraph = OrderedDict()
        temp_paragraph["type"] = "paragraph"
        temp_paragraph["value"] = i[0].attrib["Value"]
        temp_paragraph["children"] = []
        for j in i[0]:
            temp_line = OrderedDict()
            temp_line["type"] = "line"
            temp_line["value"] = j.attrib["Value"]
            zone = [[int(j.attrib["Left"]), int(j.attrib["Top"])],
                [int(j.attrib["Right"]), int(j.attrib["Top"])],
                [int(j.attrib["Right"]), int(j.attrib["Bottom"])],
                [int(j.attrib["Left"]), int(j.attrib["Bottom"])]]
            temp_line["zone"] = zone
            temp_paragraph["children"].append(temp_line)
        new_data["children"].append(temp_paragraph)

    with open(args_dict["output_json"], 'w') as outfile:
        json.dump(new_data, outfile, ensure_ascii=False)
