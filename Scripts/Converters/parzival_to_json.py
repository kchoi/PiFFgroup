import json
import argparse
from collections import OrderedDict
from pathlib import Path

if __name__ == "__main__":

    description = ('generate a JSON of the RODRIGOs txts')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-folder', help='path of the folder containing the .txt to convert')
    parser.add_argument('--output-json', help='path to save the new RODRIGO json')
    args = parser.parse_args()



    new_data = OrderedDict()
    new_data["type"] = "corpus"
    new_data["url"] = "http://www.fki.inf.unibe.ch/databases/iam-historical-document-database/parzival-database"
    new_data["id"] = "Parzival"
    new_data["children"] = []

    with open(args.input_folder+'transcription.txt') as f:
        content = f.readlines()

    with open(args.input_folder+'word_labels.txt') as f:
        words = f.readlines()

    current_line = words[0].split(' ')[0][:-3]
    line_id = current_line
    current_page = content[0].split(' ')[0][:-5]
    temp_page = OrderedDict()
    temp_page['type'] = 'page'
    temp_page['value'] = ''
    temp_page['path'] = 'data/page_images/' + current_page + '.jpg'
    temp_page['children'] = []
    word_index = 0
    for line in content:
        tmp = line.split(' ')[:2]
        ref = tmp[0]
        txt = tmp[1].replace('-','').replace('|eq','|-').replace('|pt','|.').replace('|',' ')
        temp_line = OrderedDict()
        temp_line['type'] = 'line'
        temp_line['value'] = txt
        temp_line['path'] = 'data/line_images_normalized/' + ref + '.png'
        temp_line['children'] = []
        current_line = line_id
        while current_line == line_id:
            temp_word = OrderedDict()
            temp_word['type'] = 'word'
            temp_word['value'] = words[word_index].split(' ')[1]
            temp_word['path'] = 'data/word_images_normalized/'+words[word_index].split(' ')[0]+'.png'
            temp_line['children'].append(temp_word)
            if word_index+1 >= len(words):
                break
            else:
                word_index += 1
                line_id = words[word_index].split(' ')[0][:-3]

        page_id = ref[:-5]
        if current_page == page_id:
            temp_page['value'] += txt + '\n'
            temp_page['children'].append(temp_line)
        else:
            current_page = page_id
            new_data['children'].append(temp_page)
            del temp_page
            temp_page = OrderedDict()
            temp_page['type'] = 'page'
            temp_page['value'] = txt + '\n'
            temp_page['path'] = 'data/page_images/' + current_page + '.jpg'
            temp_page['children'] = []
            temp_page['children'].append(temp_line)
    new_data['children'].append(temp_page)

    with open(args.output_json, 'w') as outfile:
        json.dump(new_data, outfile, ensure_ascii=False)
