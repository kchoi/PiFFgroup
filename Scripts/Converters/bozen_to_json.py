import json
import argparse
import sys
import xml.etree.ElementTree as et
from pathlib import Path
from collections import OrderedDict

if __name__ == "__main__":

    description = ('generate a JSON of the Bozen XMLs')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-folder', help='path of the folder containing BOZEN XMLs to convert')
    parser.add_argument('--output-json', help='path to save the new BOZEN json')
    args = parser.parse_args()

    new_data = OrderedDict()
    new_data["type"] = "corpus"
    new_data["url"] = "https://zenodo.org/record/218236#.WX8qQoiGOMp"
    new_data["id"] = "BOZEN"
    new_data["children"] = []

    sets = ['Training', 'Validation']
    for data in sets:

        pathlist = Path(args.input_folder).glob(data+'/*.xml')
        for path in sorted(pathlist):
            try:
                path_in_str = str(path)
                doc = et.parse(path_in_str).getroot()
                path_in_str = path_in_str.split('/')[2][:-4]
                temp_page = OrderedDict()
                temp_page["type"] = "page"
                temp_page["value"] = ""
                temp_page['path'] = data+'/Images/'+path_in_str+'.jpg'
                temp_page["children"] = []
                for elo in doc.iter('{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}TextLine'):
                    i = 0
                    tmp = elo.find('{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}TextEquiv')
                    txt = tmp.find('{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}Unicode').text
                    coords = elo.find('{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}Coords').attrib['points']
                    if txt == None or coords == None:
                        continue
                    coords = coords.split(' ')
                    for i in range(len(coords)):
                        coords[i] = coords[i].split(',')
                    temp_line = OrderedDict()
                    temp_line['type'] = 'line'
                    temp_line['value'] = txt
                    temp_line['zone'] = coords
                    temp_page['children'].append(temp_line)
                    temp_page['value'] += txt + '\n'
                new_data['children'].append(temp_page)

            except et.ParseError:
                continue

    with open(args.output_json, 'w') as outfile:
        json.dump(new_data, outfile, ensure_ascii=False)
