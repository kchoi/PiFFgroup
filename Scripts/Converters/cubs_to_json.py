"""
    This script transform the CUBS_IBM into a JSON
    with the values and characteristics for the pages
"""

import os
import json
import argparse
import sys
from pathlib import Path
from collections import OrderedDict

if __name__ == "__main__":

    description = ('generate a JSON of the CUBS_IBM')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-folder', help='path of the folder containing CUBS_IBM\'s .txt to convert')
    parser.add_argument('--output-json', help='path to save the new CUBS json')
    args = parser.parse_args()
    args_dict = args.__dict__

    pathlist = Path(args_dict['input_folder']).glob('**/*.txt')
    new_data = OrderedDict()
    new_data['type'] = 'corpus'
    new_data['url'] = "https://cubs.buffalo.edu/hwdata/license-agreement"
    new_data['id'] = 'CUBS_IBM'
    new_data['children'] = []

    for path in sorted(pathlist):
        path_in_str = str(path)
        with open(path_in_str, errors='ignore') as f:
            content = f.read()
        temp_page = OrderedDict()
        temp_page['type'] = 'page'
        temp_page['value'] = content
        new_data['children'].append(temp_page)

    with open(args_dict["output_json"], 'w') as outfile:
        json.dump(new_data, outfile, ensure_ascii=False)
