import json
import argparse
import sys
from collections import OrderedDict
from pathlib import Path
from pprint import pprint

if __name__ == "__main__":

    description = ('generate a JSON of the BENTHAMs txts')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-folder', help='path of the folder containing the .txt to convert')
    parser.add_argument('--output-json', help='path to save the new BENTHAM json')
    args = parser.parse_args()


    new_data = OrderedDict()
    new_data["type"] = "corpus"
    new_data["url"] = "https://zenodo.org/record/248733#.WXsVW9Pyg_U"
    new_data["id"] = "BENTHAM"
    new_data["children"] = []

    # FIRST BATCH
    pathlist = Path(args.input_folder).glob('Bentham1/*.txt')
    i = 0
    current_page = 'non'
    line_list = []
    for content in sorted(pathlist):
        #print('\n\n',content,'\n\n')
        tmp = str(content).split('/')[2].split('_')[:3]
        tmp = tmp[0]+'_'+tmp[1]+'_'+tmp[2]
        if i == 0:
            current_page = tmp
            temp_page = OrderedDict()
            temp_page['type'] = 'page'
            temp_page['value'] = ''
            temp_page['path'] = 'TRAIN/contestHTRtS/BenthamData/1stBatch/Images/Pages/'+current_page + '.jpg'
            temp_page['children'] = []
            i += 1
        with open(str(content)) as f:
            txt = f.read()
            temp_line = OrderedDict()
            temp_line['type'] = 'line'
            tmp_str = txt
            temp_line['value'] = tmp_str.replace('\n','')
            temp_line['path'] = 'TRAIN/contestHTRtS/BenthamData/1stBatch/Images/Lines/' + str(content)[18:-4] + '.png'
        if current_page == tmp:
            temp_page['value'] += txt
            temp_page['children'].append(temp_line)
        else:
            current_page = tmp
            new_data['children'].append(temp_page)
            del temp_page
            temp_page = OrderedDict()
            temp_page['type'] = 'page'
            temp_page['value'] = txt
            temp_page['path'] = 'TRAIN/contestHTRtS/BenthamData/1stBatch/Images/Pages/'+current_page + '.jpg'
            temp_page['children'] = []
            temp_page['children'].append(temp_line)
    new_data['children'].append(temp_page)

    #SECOND BATCH
    pathlist = Path(args.input_folder).glob('Bentham2/*.txt')
    for content in sorted(pathlist):
        with open(str(content)) as f:
            txt = f.read()
            temp_page = OrderedDict()
            temp_page['type'] = 'page'
            temp_page['value'] = txt
            tmp = str(content).split('/')[2][:-4]
            temp_page['path'] = 'TRAIN/contestHTRtS/BenthamData/2ndBatch/Images/Pages/'+tmp + '.jpg'
            new_data['children'].append(temp_page)

    #TEST BATCH
    pathlist = Path(args.input_folder).glob('Bentham3/*.dat')
    current_page = 'non'
    i = 0
    temp_page = OrderedDict()
    temp_page['type'] = 'page'
    temp_page['value'] = ''
    temp_page['path'] = ''
    temp_page['children'] = []
    for content in sorted(pathlist):
        with open(str(content)) as f:
            the_folder = str(content).split('/')[2][:-17]
            txt_lines = f.readlines()
        for a_line in txt_lines:
            txt = a_line.split('\t',1)[1]
            ref = a_line.split('\t',1)[0]
            tmp = ref[:11]
            if i == 0:
                temp_page['path'] = 'TEST/'+the_folder+'/Images/Pages/'+ref[:11]+'.jpg'
                i += 1
            temp_line = OrderedDict()
            temp_line['type'] = 'line'
            tmp_str = txt
            temp_line['value'] = tmp_str.replace('\n','')
            temp_line['path'] = 'TEST/'+the_folder+'/Images/Lines/' +ref+ '.png'
            if current_page == tmp:
                temp_page['value'] += txt
                temp_page['children'].append(temp_line)
            else:
                current_page = tmp
                new_data['children'].append(temp_page)
                del temp_page
                temp_page = OrderedDict()
                temp_page['type'] = 'page'
                temp_page['value'] = txt
                temp_page['path'] = 'TRAIN/contestHTRtS/BenthamData/1stBatch/Images/Pages/'+current_page + '.jpg'
                temp_page['children'] = []
                temp_page['children'].append(temp_line)
        new_data['children'].append(temp_page)




    with open(args.output_json, 'w') as outfile:
        json.dump(new_data, outfile, ensure_ascii=False)
