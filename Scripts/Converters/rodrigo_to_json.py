import json
import argparse
from collections import OrderedDict
from pathlib import Path

if __name__ == "__main__":

    description = ('generate a JSON of the RODRIGOs txts')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-folder', help='path of the folder containing the .txt to convert')
    parser.add_argument('--output-json', help='path to save the new RODRIGO json')
    args = parser.parse_args()



    new_data = OrderedDict()
    new_data["type"] = "corpus"
    new_data["url"] = ""
    new_data["id"] = "RODRIGO"
    new_data["children"] = []
    pathlist = Path(args.input_folder).glob('**/*.txt')
    for content in pathlist:
        print('\n\n',content,'\n\n')
        with open(str(content)) as f:
            txt = f.readlines()
        i = 0
        for line in txt:
            if line == ' ':
                print("LOLLOL")
            temp_line = OrderedDict()
            temp_line['type'] = 'line'
            tmp_str = line
            temp_line['value'] = tmp_str.replace('\n','').replace('$','-').replace('␣','').replace('‿',' ')
            temp_line['path'] = 'png/' + str(content)[9:-4] + '_' +str(i).zfill(2) + '.png'
            i += 1
            print(temp_line['path'])
            print(temp_line['value'])
            new_data['children'].append(temp_line)

    with open(args.output_json, 'w') as outfile:
        json.dump(new_data, outfile, ensure_ascii=False)
