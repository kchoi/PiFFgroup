import json
import argparse
import sys
import os
import xml.etree.ElementTree as et
from pathlib import Path
from collections import OrderedDict

def get_bounding_box(coordinates):
    left, up, right, down = 9999, 9999, 0, 0
    for point in coordinates:
        x = int(point.split(',')[0])
        y = int(point.split(',')[1])
        if x < left:
            left = x
        if y < up:
            up = y
        if x > right:
            right = x
        if y > down:
            down = y
    res = [
        [left,up],
        [right,up],
        [right,down],
        [left,down]
    ]
    return res

if __name__ == "__main__":

    description = ('generate a JSON of the Konzilsprotokolle XMLs')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-folder', help='path of the folder containing KONZINLSPROTOKOLLE XMLs to convert')
    parser.add_argument('--output-json', help='path to save the new KONZINLSPROTOKOLLE json')
    args = parser.parse_args()

    new_data = OrderedDict()
    new_data["type"] = "corpus"
    new_data["url"] = "https://zenodo.org/record/215383#.WXsV19Pyg_U"
    new_data["id"] = "KONZINLSPROTOKOLLE"
    new_data["children"] = []

    sets = next(os.walk(args.input_folder))[1]
    for data in sets:
        #data = sets[3]
        pathlist = Path(args.input_folder).glob(data+'/*.xml')
        for path in sorted(pathlist):
            try:
                path_in_str = str(path)
                doc = et.parse(path_in_str).getroot()
                path_in_str = path_in_str.split('/')[2][:-4]
                temp_page = OrderedDict()
                temp_page['type'] = 'page'
                temp_page['value'] = ''
                temp_page['path'] = 'data/Greifswald_Alvermann/'+data+'/'+path_in_str+'.tif'
                try:
                    tmp = doc.find("{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}Page")
                    tmp = tmp.find("{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}TextRegion")
                    if tmp != None:
                        if 'TextEquiv' not in str(tmp[:]):
                            new_data['children'].append(temp_page)
                            continue
                        else:
                            temp_page['children'] = []
                            for element in tmp[:]:
                                if 'TextEquiv' in str(element):
                                    temp_page['value'] = element[0].text
                                if 'TextLine' in str(element):
                                    coords = element[0].attrib['points']
                                    if 'TextEquiv' in str(element[:]):
                                        if 'TextEquiv' in str(element[1]):
                                            txt = element[1][0].text
                                        else:
                                            txt = element[2][0].text
                                    else:
                                        txt = ''
                                    temp_line = OrderedDict()
                                    temp_line['type'] = 'line'
                                    temp_line['value'] = txt
                                    coords = coords.split(' ')
                                    coords = get_bounding_box(coords)
                                    temp_line['zone'] = coords
                                    temp_page['children'].append(temp_line)
                            new_data['children'].append(temp_page)
                except IndexError:
                    new_data['children'].append(temp_page)
                    continue
            except et.ParseError:
                continue

    with open(args.output_json, 'w') as outfile:
        json.dump(new_data, outfile, ensure_ascii=False)
