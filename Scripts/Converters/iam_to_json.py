# coding: utf8
# authors : Christopher Kermorvant, Andres Rojas
# 2017 -  TEKLIA

"""
    This script transform the IAM XML into a JSON
    with the values and characteristics for
    Lines and Paragraphs
"""

import os
import json
import argparse
import sys
import xml.etree.ElementTree as et
from pathlib import Path
from collections import OrderedDict

if __name__ == "__main__":

    description = ('generate a JSON of the IAM XMLs')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-folder', help='path of the folder containing IAM XMLs and lines.txt to convert')
    parser.add_argument('--output-json', help='path to save the new IAM json')
    args = parser.parse_args()
    args_dict = args.__dict__

    lines_txt_directory = args_dict["input_folder"] + "lines.txt"
    if "/lines" not in lines_txt_directory:
        lines_txt_directory = args_dict["input_folder"] + "/lines.txt"

    with open(lines_txt_directory) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    line_counter = 0
    while "#" in content[line_counter].split()[0]:
        line_counter += 1

    new_data = OrderedDict()
    new_data["type"] = "corpus"
    new_data["url"] = "http://www.fki.inf.unibe.ch/databases/iam-handwriting-database"
    new_data["id"] = "IAM"
    new_data["children"] = []
    pathlist = Path(args_dict["input_folder"]).glob('**/*.xml')
    for path in sorted(pathlist):
        path_in_str = str(path)
        doc = et.parse(path_in_str).getroot()
        temp_paragraph = OrderedDict()
        temp_paragraph["type"] = "paragraph"
        temp_paragraph["value"] = ""
        temp_paragraph["path"] = ""
        if doc.attrib['id'][0] <= 'd':
            temp_paragraph["path"] = "forms/formsA-D/" + doc.attrib['id'] + ".png"
        elif doc.attrib['id'][0] >= 'e' and doc.attrib['id'][0] <= 'h':
            temp_paragraph["path"] = "forms/formsE-H/" + doc.attrib['id'] + ".png"
        elif doc.attrib['id'][0] >= 'i':
            temp_paragraph["path"] = "forms/formsI-Z/" + doc.attrib['id'] + ".png"
        temp_paragraph["zone"] = []
        temp_paragraph["children"] = []
        x1_par, y1_par = 9999, 9999
        x2_par, y2_par = 0, 0
        for i in doc[1]:
            temp_paragraph["value"] = temp_paragraph["value"] + i.attrib["text"].replace('&quot;','"') + "\n"
            temp_line = OrderedDict()
            temp_line["type"] = "line"
            temp_line["value"] = i.attrib["text"].replace('&quot;','"')
            tmp = i.attrib['id'].split('-')
            temp_line['path'] = 'lines/'+tmp[0]+'/'+tmp[0]+'-'+tmp[1]+'/'+i.attrib['id']+'.png' 
            temp_zone = content[line_counter].split()
            if x1_par > int(temp_zone[4]):
                x1_par = int(temp_zone[4])
            if y1_par > int(temp_zone[5]):
                y1_par = int(temp_zone[5])
            if x2_par < int(temp_zone[4])+int(temp_zone[6]):
                x2_par = int(temp_zone[4])+int(temp_zone[6])
            if y2_par < int(temp_zone[5])+int(temp_zone[7]):
                y2_par = int(temp_zone[5])+int(temp_zone[7])
            zone = [[int(temp_zone[4]), int(temp_zone[5])],
                [int(temp_zone[4])+int(temp_zone[6]),int(temp_zone[5])],
                [int(temp_zone[4])+int(temp_zone[6]),int(temp_zone[5])+int(temp_zone[7])],
                [int(temp_zone[4]),int(temp_zone[5])+int(temp_zone[7])]]
            temp_line["zone"] = zone
            temp_line["children"] = []
            for j in i:
                try:
                    word_text = j.attrib['text'].replace('&quot;','"')
                    temp_word_file = j.attrib['id'].split('-')
                    word_file = "words/%s/%s-%s/%s-%s-%s-%s.png" % (temp_word_file[0],temp_word_file[0],temp_word_file[1],temp_word_file[0],temp_word_file[1],temp_word_file[2],temp_word_file[3])
                    temp_word = OrderedDict()
                    temp_word["type"] = "word"
                    temp_word["value"] = word_text
                    temp_word["path"] = word_file
                    temp_line["children"].append(temp_word)
                except KeyError:
                    continue
            temp_paragraph["zone"] = [[x1_par, y1_par],
                                      [x2_par, y1_par],
                                      [x2_par, y2_par],
                                      [x1_par, y2_par]]
            temp_paragraph["children"].append(temp_line)
            line_counter += 1
        new_data["children"].append(temp_paragraph)

    with open(args_dict["output_json"], 'w') as outfile:
        json.dump(new_data, outfile, ensure_ascii=False)
