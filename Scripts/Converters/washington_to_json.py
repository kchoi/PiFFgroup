import json
import argparse
from collections import OrderedDict

if __name__ == "__main__":

    description = ('generate a JSON of the WASHINGTONs txts')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-folder', help='path of the folder containing the .txt to convert')
    parser.add_argument('--output-json', help='path to save the new WASHINGTON json')
    args = parser.parse_args()

    symbols = {
    's_1' : '1',
    's_2' : '2',
    's_3' : '3',
    's_4' : '4',
    's_5' : '5',
    's_6' : '6',
    's_7' : '7',
    's_8' : '8',
    's_9' : '9',
    's_0' : '0',
    's_pt' : '.',
    's_cm' : ',',
    's_mi' : '-',
    's_sq' : ';',
    's_GW' : 'GW',
    's_qo' : ':',
    's_et' : 'V',
    #'s_s' : 's'
    }

    def replace_symbols(stringo):
        global symbols
        tmp = stringo
        for key, value in symbols.items():
            tmp = tmp.replace(key,value)
        tmp = tmp.replace('s_s','s')
        return tmp

    words_txt = args.input_folder + 'word_labels.txt'
    lines_txt = args.input_folder + 'transcription.txt'
    line_prefix = 'data/line_images_normalized/'
    word_prefix = 'data/word_images_normalized/'

    with open(words_txt) as f:
        words = f.readlines()

    with open(lines_txt) as f:
        lines = f.readlines()

    new_data = OrderedDict()
    new_data["type"] = "corpus"
    new_data["url"] = "http://www.fki.inf.unibe.ch/databases/iam-historical-document-database/washington-database"
    new_data["id"] = "WASHINGTON"
    new_data["children"] = []
    for line in lines:
        temp_line = OrderedDict()
        temp_line['type'] = 'line'
        tmp_str = line.split(' ')[1].replace('|',' ').replace('-','')
        temp_line['value'] = replace_symbols(tmp_str)
        temp_line['path'] = line_prefix + line.split(' ')[0] + '.png'
        temp_line['children'] = []
        tmp_pos = line.split(' ')[0]
        words_in_line = []
        for word in words:
            tmp_word_pos = word.split(' ')[0]
            if tmp_pos in tmp_word_pos:
                words_in_line.append(word)
        for word in words_in_line:
            tmp_word = OrderedDict()
            tmp_word['type'] = "word"
            tmp_word['value'] = replace_symbols(word.split(' ')[1].replace('-',''))
            tmp_word['path'] = word_prefix + word.split(' ')[0] + '.png'
            temp_line['children'].append(tmp_word)
        new_data['children'].append(temp_line)

    with open(args.output_json, 'w') as outfile:
        json.dump(new_data, outfile, ensure_ascii=False)
