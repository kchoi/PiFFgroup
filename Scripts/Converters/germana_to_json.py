import json
import argparse
from collections import OrderedDict
from pathlib import Path

if __name__ == "__main__":

    description = ('generate a JSON of the GERMANAs txts')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-folder', help='path of the folder containing the .txt to convert')
    parser.add_argument('--output-json', help='path to save the new GERMANA json')
    args = parser.parse_args()



    new_data = OrderedDict()
    new_data["type"] = "corpus"
    new_data["url"] = ""
    new_data["id"] = "GERMANA"
    new_data["children"] = []
    pathlist = Path(args.input_folder).glob('**/*.txt')
    for content in pathlist:
        with open(str(content)) as f:
            page = f.read()
        temp_page = OrderedDict()
        temp_page['type'] = 'page'
        temp_page['value'] = page
        temp_page['path'] = 'images/' + str(content)[9:-4] + '.png'
        new_data['children'].append(temp_page)

    with open(args.output_json, 'w') as outfile:
        json.dump(new_data, outfile, ensure_ascii=False)
