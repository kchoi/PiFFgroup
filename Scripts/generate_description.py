# coding: utf8
# authors : Christopher Kermorvant, Andres Rojas
# 2017 -  TEKLIA

import argparse
import logging
import sys



COMMAND_NAME = os.path.basename(__file__)
logger = logging.getLogger(COMMAND_NAME)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logger.addHandler(ch)
# uncomment if you need to log into a file
#fh = logging.FileHandler(COMMAND_NAME+'.txt')
#logger.addHandler(fh)

if __name__ == "__main__":
    
   description = ('generate the description in .md format from the Database directory')
   parser = argparse.ArgumentParser(description=description)
   parser.add_argument('--input-dir', help='root directory to explore')


   args = parser.parse_args()

   
 