import argparse
import json
import sys

def recursive_trn(data,level,f):
    global i
    for children in data['children']:
        if children['type'] == level:
            f.write(children['value'].replace('\n',' ') + ' ('+str(i)+')\n')
            i += 1
        else:
            if children['type'] == 'word':
                sys.exit('levels: document, paragraph, line, word ... try again')
            recursive_trn(children,level,f)

if __name__ == "__main__":
    description = 'create a trn file with the specified level of database JSON'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input-json',required=True, help='path/to/json/file.json')
    parser.add_argument('--output-trn',default='database.trn')
    parser.add_argument('--level',required=True, help='document,paragraph,line,word')
    args = parser.parse_args()

    with open(args.input_json) as f:
        data = json.load(f)

    with open(args.output_trn,'w') as f:
        i = 0
        recursive_trn(data,args.level,f)
