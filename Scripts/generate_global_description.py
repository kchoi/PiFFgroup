import os
import sys
import glob

with open('../Global_statistics.md','w') as md:


    md.write('|Base'.ljust(20)+'|'+'Pages'.ljust(10)+'|'+'Paragraphs'.ljust(10)+'|'+'Lines'.ljust(10)+'|'+'Words'.ljust(10)+'|'+'Characters'.ljust(10)+'|\n')
    md.write('|'+ '-'*20+'|' + ('-'*9+':|')*5 +'\n')
    total_pages = 0
    total_paragraphs = 0
    total_lines = 0
    total_words = 0
    total_characters = 0
    for filename in glob.glob('../Databases/**/*.md'):
        with open(filename) as f:
            res = f.readlines()
        for i in range(len(res)):
            res[i] = res[i].replace('\n','')
        tmp_pag = int(res[3].split(':')[1])
        total_pages += tmp_pag
        tmp_par = int(res[4].split(':')[1])
        total_paragraphs += tmp_par
        tmp_lin = int(res[5].split(':')[1])
        total_lines += tmp_lin
        tmp_wor = int(res[6].split(':')[1])
        total_words += tmp_wor
        tmp_cha = int(res[7].split(':')[1])
        total_characters += tmp_cha
        md.write('|'+res[0].split(' ')[2].ljust(20)+'|'+str(tmp_pag).ljust(10)+'|'+str(tmp_par).ljust(10)+'|'+str(tmp_lin).ljust(10)+'|'+str(tmp_wor).ljust(10)+'|'+str(tmp_cha).ljust(10)+'|\n')

    md.write('|TOTAL'.ljust(20)+'|'+str(total_pages).ljust(10)+'|'+str(total_paragraphs).ljust(10)+'|'+str(total_lines).ljust(10)+'|'+str(total_words).ljust(10)+'|'+str(total_characters).ljust(10)+'|')
