# Specifications

Each element has these attributes:

- type: paragraph, line, word, annotation, title... (what you need)
- value: a string or list of strings for the textual content present in this element
- zone/polygon: a list of coordinates for the location of the element on the image
- id: unique identifier of the object
- children: list of sub elements (or their id)
- parent: id parent if a graph structure is needed
- url: path to an image


Value, zone, children, id and url are optional.

The json file can be just a list of these elements or structured with one root containing:

- meta: with attribute "type", "piff_version", "url", "date", "id" concerning globaly the document or document set.
- location: list of zone/polygon with id 
- data: list of element linked to the previous locations using the ids.
