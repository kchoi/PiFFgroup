import json
import argparse

def write(elt) :
    
    
    
    l=elt["polyline"].split(";")
    
    pi=l[0].split(",")
    pf=l[len(l)-1].split(",")
    
    DMOS="(coor"+pi[0]+" "+pi[1]+")"
     
    for p in l[1:] :
        p1=p.split(",")
        DMOS+=",(coor"+p1[0]+" "+p1[1]+")"
        
    DMOS+="] ))) (coorDeb " + pi[0] +" " +pi[1] +") (coorFin "+ pf[0]+ " " +pf[1]+" ) ) \n  "
    return DMOS



if __name__=='__main__' :
    
    parser=argparse.ArgumentParser()
    
    parser.add_argument('-i', '--input_file', required=True, type=str, default=None,
                        help='PIFF file')
    
    parser.add_argument('-r','--res' , required=True, type=str, default=None,
                        help='name of the DMOS output file')
      
    
    
    args=vars(parser.parse_args())
    
    input_file=args.get('input_file')
    
    res=args.get('res')
    
    
    with open(input_file) as json_data:
        data_dict = json.load(json_data)
    

    DMOS="(structDMOS _ [] ["

    DMOS+=" (segH [] (etiqNomImage (etiqNom 6) (structMem (ligne_dhSegment ["
  
    DMOS+=write(data_dict["data"][0])
               
    for elt in data_dict["data"][1:] :
        
    
        DMOS+=", (segH [] (etiqNomImage (etiqNom 6) (structMem (ligne_dhSegment ["
        DMOS+=write(elt)
        


    DMOS+=" ]  []). \n"      

   
    with open(res, "w") as f:
        f.write(DMOS)   




   
   


    


    
 
    

    
    

     
 
    
    
