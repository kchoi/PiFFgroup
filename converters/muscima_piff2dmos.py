from typing import List
import numpy as np
import argparse
import Piff


class PiffDMOS(object):

    """Translate Piff <-> DMOS"""

    def __init__(self):
        """Constructor """

    def polygon2bbox(self, polygon: List[List[str]]) -> List[int]:
        # left, top, right, bottom
        bbox = [np.inf, np.inf, 0, 0]
        for point in polygon:
            x, y = int(point[0]), int(point[1])
            bbox[0] = min(bbox[0], x)
            bbox[1] = min(bbox[1], y)
            bbox[2] = max(bbox[2], x)
            bbox[3] = max(bbox[3], y)
        return bbox

    def write_classEtiqs(self, datas):
        classEtiqs = "[" + datas[0]['value']
        for data in datas[1:]:
            classEtiqs += " " + data['value']
        classEtiqs += "]"
        return classEtiqs

    def write_coord(self, polygon):
        [x1, y1, x2, y2] = self.polygon2bbox(polygon)
        coord = "(coorDeb " + str(x1) + " " +  str(y1) + ") "
        coord += "(coorFin " + str(x2) + " " +  str(y2) + ")"
        return coord

    def write_cxxOrSeg(self, piff, loc):
        datas = piff.get_data_by_locid(loc['id'])
        cxxOrSeg = "(" + loc['type'] + " "
        cxxOrSeg += self.write_classEtiqs(datas) + " "
        cxxOrSeg += "(etiqNomImage (etiqNom " + str(loc['id']) + ") _) "
        cxxOrSeg += self.write_coord(loc['polygon']) + ")"
        return cxxOrSeg

    def write_structImage(self, piff):
        structImage = ""
        structImage += "(structImage _ ["
        structImage += self.write_cxxOrSeg(piff, piff.location[0])
        for loc in piff.location[1:]:
            structImage += " " + self.write_cxxOrSeg(piff, loc)
        return structImage

    def transform(self, piff_file: str, dmos_file: str) -> None:
        piff = Piff.Piff("", "", "", "", "")
        piff.load(piff_file)
        with open(dmos_file, "w") as f:
            f.write(self.write_structImage(piff))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert Piff file containing muscima++ dataset information to DMOS Perceptif layer")
    parser.add_argument("filename", help="PIvot File Format PIFF file")
    parser.add_argument("-o", "--output-file",
                        dest="outfile",
                        type=str,
                        default='./output.mem',
                        help="name of the output dmos file",)
    args = parser.parse_args()
    piffDmos = PiffDMOS()
    piffDmos.transform(args.filename, args.outfile)
