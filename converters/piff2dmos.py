import argparse

import sys
import os
import time
import csv
import json
import gzip, pickle

import scipy.misc
import Piff


def main(args):
    
    piff = Piff.Piff()
    piff.load(args.filename)
    meta = piff.meta
    ID = meta['id']
    idcard = str(ID[6:8]) + str(ID[9:13])

    # EXAMPLE :
    # (structImage _ LstCC LstSegH LstSegV)
    # avec LstCC :
    #    [(ccx [Digit] (etiqNomImage (etiqNom 1) _ ) (coorDeb 0 480) (coorFin 28 508))
    #     (ccx [Digit] (etiqNomImage (etiqNom 2) _) (coorDeb 436 2800) (coorFin 452 2812)) ]
    DMOS = "(structImage _ ["
    for elt in piff.location:
        l_type = elt['type']        
        l_id = elt['id']
        p = l_id[len('EltStruct_'):]
        l_poly = elt['polygon']
        x1, y1, x2, y2 = l_poly[0][0], l_poly[0][1], l_poly[1][0], l_poly[3][1]
        DMOS += "("
        DMOS += "ccx ["+l_type+"] "
        DMOS += "(etiqNomImage (etiqNom " + str(p)+") _) "
        DMOS += "(coorDeb " + str(x1)+ " " + str(y1) + ") "
        DMOS += "(coorFin " + str(x2)+ " " + str(y2) + ")"
        DMOS += ") "
    DMOS += "] LstSegH LstSegV)"
    text_file = open(args.outfile, "w")
    text_file.write(DMOS)
    text_file.close()

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description="Convert Piff file to DMOS Perceptif layer")
    parser.add_argument("filename", help="PIvot File Format PIFF file")
    parser.add_argument("-o", "--output-file",
                        dest="outfile",
                        type=str,
                        default='./output.mem',
                        help="name of the output dmos file",)
    args = parser.parse_args()
    main(args)
