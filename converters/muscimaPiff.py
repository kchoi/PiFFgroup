import argparse
from typing import List
import numpy as np
from muscima.cropobject import CropObject
import muscima.io
import Piff
import time


class MuscimaPiff(object):

    """Translate Muscima <-> Piff"""

    def __init__(self):
        """Constructor"""

    def fit(self, translate_class_file: str) -> None:
        data = np.genfromtxt(translate_class_file, delimiter=",",
                             skip_header=True, dtype=str)
        data = [x for x in data if x[1] != ""]
        self.translator = {x[0]: [x[1], x[2]] for x in data}

    @property
    def inverse_translator(self):
        keys = [x[1] for x in self.translator.values()]
        values = list(self.translator.keys())
        return {key: value for key, value in zip(keys, values)}

    def transform(self, muscimapp_file: str, image_file: str,
                  piff_file: str) -> None:
        muscima_data = muscima.io.parse_cropobject_list(muscimapp_file)
        piff = Piff.Piff(muscima_data[0].parse_uid()[0], "version 0",
                         image_file, time.ctime(),
                         muscima_data[0].parse_uid()[1])
        for obj in muscima_data:
            if obj.clsname in self.translator.keys():
                typename = self.translator[obj.clsname][0]
                classname = self.translator[obj.clsname][1]
                polygon = [[str(obj.left), str(obj.top)],
                           [str(obj.left + obj.width), str(obj.top)],
                           [str(obj.left + obj.width),
                            str(obj.top + obj.height)],
                           [str(obj.left), str(obj.top + obj.height)]]
                piff.add_elt_data(obj.objid, obj.objid, classname, None,
                                  typename)
                piff.add_elt_location(obj.objid, typename, polygon)
        piff.save(piff_file)

    def polygon2bbox(self, polygon: List[List[str]]) -> List[int]:
        # top, left, right, bottom
        bbox = [np.inf, np.inf, 0, 0]
        for point in polygon:
            x, y = int(point[0]), int(point[1])
            bbox[0] = min(bbox[0], y)
            bbox[1] = min(bbox[1], x)
            bbox[2] = max(bbox[2], x)
            bbox[3] = max(bbox[3], y)
        # bbox: [top, left, width, height]
        bbox[2] -= bbox[1]
        bbox[3] -= bbox[0]
        return bbox

    def inverse_transform(self, piff_file: str, muscimapp_file: str) -> None:
        piff = Piff.Piff("", "", "", "", "")
        piff.load(piff_file)
        crop_object_list: List[CropObject] = []
        for loc in piff.location:
            data = piff.get_data_by_locid(loc['id'])[0]
            clsname = self.inverse_translator[data['value']]
            crop_object = CropObject(
                loc['id'], clsname,
                *self.polygon2bbox(loc['polygon']))
            crop_object.set_dataset(piff.meta['type'])
            crop_object.set_doc(piff.meta['id'])
            crop_object_list.append(crop_object)
        with open(muscimapp_file, "w") as f:
            f.write(muscima.io.export_cropobject_list(crop_object_list))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("translation_file", type=str,
                        help="Path to translation file")
    parser.add_argument("muscimapp_file", type=str,
                        help="Path to muscima xml file")
    parser.add_argument("image_file", type=str,
                        help="Path to image file")
    parser.add_argument("piff_file", type=str,
                        help="Path to output piff file")
    args = parser.parse_args()

    mupiff = MuscimaPiff()
    mupiff.fit(args.translation_file)
    mupiff.transform(args.muscimapp_file, args.image_file, args.piff_file)
    # mupiff.inverse_transform(args.piff_file, "test_muscima_gen.xml")
