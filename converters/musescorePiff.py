import argparse
import xml.etree.ElementTree as et
from . import Piff
import time
import os
import numpy as np

class MusescorePiff(object):

    """Translate Musescore xml -> Piff (not musicxml)"""

    def fit(self, translate_class_file: str) -> None:
        data = np.genfromtxt(translate_class_file, delimiter=",",
                             skip_header=True, dtype=str)
        data = [x for x in data]
        self.translator = {x[0]: x[1] for x in data}

    def transform(self, musecore_file, output, image):
        musescore_data = et.parse(musecore_file).getroot()
        piff = Piff.Piff(None, "version 0",
                         image, time.ctime(),
                         os.path.basename(musecore_file)[:-4])
        i = 0
        for symbol in musescore_data.findall('Symbol'):
            shape_name = symbol.get('shape')
            if shape_name in self.translator.keys():
                classname = self.translator[shape_name]
                polygon = self.__generatePoly(symbol)
                piff.add_elt_data(i, i, classname, None, classname)
                piff.add_elt_location(i, classname, polygon)
                i += 1
        piff.save(output)

    def __generatePoly(self, symbol):
        x, y, w, h = self.__findCoordinates(symbol.find('Bounds'))
        return [[x, y], [x + w, y], [x + w, y + h], [x, y + h]]
        
    def __findCoordinates(self, shape):
        return float(shape.get('x')), float(shape.get('y')), float(shape.get('w')), float(shape.get('h'))

def main(args):
    mupiff = MusescorePiff()
    mupiff.fit(args.translator)
    mupiff.transform(args.musescore_file, args.output_piff, args.image)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--musescore_file", type=str,
                        help="Path to musescore xml file")
    parser.add_argument("image", type=str,
                        help="Path to image file")
    parser.add_argument("translator", type=str,
                        help="Path to translator file")
    parser.add_argument("-o", "--output_piff", type=str, 
                        help="Path to output piff file")
    args = parser.parse_args()
    main(args)
